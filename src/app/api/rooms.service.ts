import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  constructor(private db: AngularFirestore) {}

  getRooms(){
    return this.db.collection('rooms').valueChanges();
  }
  
}
