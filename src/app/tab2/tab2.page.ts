import { Component } from "@angular/core";
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page {

  constructor(private db: AngularFirestore){}

  //Nom de la nouvelle room
  name: string;

  //Création de la référence de notre collection rooms
  ref = this.db.collection('rooms');

  //Méthode de création de la room
  addRoom() {

    //Création de l'obj de la nouvelle room 
    let newRoom = {
      name : this.name
    }

    //On post la nouvelle room dans la collection rooms
    this.ref.add(newRoom);

  }
}
